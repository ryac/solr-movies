express = require 'express'
solr = require 'solr-client'
request = require 'request'
trim = require 'trim'
_ = require 'nimble'

router = express.Router()

###
GET call to /api/movies will:
1) create a solr client and query it with q
2) simply pass the result back to the front-end
this is really just a proxy..
###
router.get '/api/movies', (req, res) ->
  client = solr.createClient('127.0.0.1', '8983', 'movies')

  query = client.createQuery().q(req.query.q)

  if (req.query.fq)
    key = req.query.fq.substr(0, req.query.fq.indexOf(':'))
    val = req.query.fq.substr(req.query.fq.indexOf(':') + 1)
    query.matchFilter(key, val)

  client.search query, (err, obj) ->
    if err
      console.log err
      res.status(400).send {success: false, message: 'something went wrong..'}
    else
      res.send obj

###
POST call to /api/movies will:
1) make a GET request to Rotten Tomatoes
   get a list of movies..
2) make another GET request for each movie
   found to only gather the genres for the
   movie..
3) once we have all info, create a solr
   client to add the all movies for indexing.
4) documents are commited after added..
eg: curl --data '{"query":"ryan"}' -H 'Content-type:application/json' http://localhost:3000/api/movies
###
router.post '/api/movies', (req, res) ->

  movieParser = {

    movies: []

    getMovieDetails: (item, callback) ->
      self = this
      console.log 'getting details for:', item.id
      opts = {
        url: 'http://api.rottentomatoes.com/api/public/v1.0/movies/' + item.id
        qs:
          apiKey: 'juv4b2rgftzy44gscu27rvny'
      }
      request opts, (err, res, body) =>
        if (!err and res.statusCode is 200)
          data = JSON.parse(trim(body))
          m = {
            id: item.id
            title: item.title
            mpaa_rating: item.mpaa_rating
            critics_score: item.ratings.critics_score
            audience_score: item.ratings.audience_score
            poster: item.posters.original
            link: item.links.alternate
            genres: data.genres
          }

          m.runtime = item.runtime if item.runtime isnt ''
          m.year = item.year if item.year isnt ''

          @movies.push m
        else
          console.log 'error here >>', err
        callback()

    sendData: ->
      res.json { success: true, numFound: movieParser.movies.length, movies: movieParser.movies }
      client = solr.createClient('127.0.0.1', '8983', 'movies')
      # client.autoCommit = true
      console.log '----------'
      console.log movieParser.movies
      console.log '----------'
      client.add movieParser.movies, (err, obj) ->
        if (err)
          console.log err
        else
          console.log 'Solr Response:', obj
          # client.commit (err,res) ->
          #   if (err) then console.log err
          #   if (res) then console.log res
  }

  opts = {
    url: 'http://api.rottentomatoes.com/api/public/v1.0/movies'
    qs:
      q: req.body.query
      apiKey: 'juv4b2rgftzy44gscu27rvny'
      page_limit: 50
  }

  request opts, (err, response, body) ->
    if (!err and response.statusCode is 200)
      data = JSON.parse(trim(body))
      fns = []
      data.movies.forEach (item) ->
        fns.push (callback) ->
          movieParser.getMovieDetails(item, callback)

      _.series fns, movieParser.sendData
    else
      console.log 'error!'

###
GET call to make a hard commit..
###
router.get '/api/movies/commit', (req, res) ->
  client = solr.createClient('127.0.0.1', '8983', 'movies')
  client.commit (err, res) ->
    if (err) then console.log err
    if (res) then console.log res

module.exports = router
