express = require 'express'
solr = require 'solr-client'
router = express.Router()

router.use (req, res, next)->
  console.log '%s %s %s', req.method, req.url, req.path
  next()

router.get '/', (req, res) ->
  # createClient(host, port, core, path, agent, secure, bigint)
  solr = solr.createClient('127.0.0.1', '8983', 'movies')
  # solr.ping (err, obj) ->
  #   if err
  #     console.log err
  #   else
  #     console.log obj

  query = solr.createQuery()
    .q('terminator')
    .start(0)
    .rows(50)

  solr.search query, (err, obj) ->
    if err
      console.log err
    else
      console.log obj.response.docs[0]


  res.render 'solr/index'

module.exports = router