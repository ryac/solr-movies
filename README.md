# Solr Movies
Search-based app using Solr to search for movies.

Global dependancies
-------------------

* gulp `npm install gulp -g`
* coffee-script `npm install coffee-script -g`
* supervisor `npm install supervisor -g`

Install dependancies
--------------------
`npm install`
`cd public && bower install`

Start app by running `npm start`