module.exports = (grunt) ->

  # Tasks
  grunt.initConfig {

    # ----------------------
    # Clean
    # ----------------------
    clean:
      src: ['public/js/app/**']
      templates: ['public/js/templates/templates.js']

    # ----------------------
    # Coffee
    # ----------------------
    coffee:
      dev:
        options:
          bare: true
        expand: true
        cwd: 'public/coffee/'
        src: ['**.coffee', '**/*.coffee']
        dest: 'public/js/app/'
        ext: '.js'

    # ----------------------
    # Watch
    # ----------------------
    watch:
      scripts:
        files: 'public/coffee/**/*.coffee'
        tasks: ['clean:src', 'coffee']
      emberTemplates:
        files: 'public/templates/**/*.hbs',
        tasks: ['clean:templates', 'emberTemplates']

    # --------------------------------------
    # Compile Handlebars templates for Ember
    # --------------------------------------
    emberTemplates:
      compile:
        options:
          templateCompilerPath: 'public/bower_components/ember/ember-template-compiler.js'
          handlebarsPath: 'public/bower_components/handlebars/handlebars.min.js'
          templateBasePath: 'public/templates'
          precompile: false
        files:
          'public/js/templates/templates.js': 'public/templates/**/*.hbs'
  }

  # Plugins
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)

  # Commands
  grunt.registerTask 'default', ['']

  return grunt