# Foundation JavaScript - documentation can be found at: http://foundation.zurb.com/docs
$ ->
  $(document).foundation()

App = Ember.Application.create
  LOG_TRANSITIONS: true

App.ApplicationAdapter = DS.RESTAdapter.extend
  namespace: 'api'

App.GenresTransform = DS.Transform.extend
  serialize: (value) ->
    return value
  deserialize: (value) ->
    return Ember.A value

App.ApplicationSerializer = DS.RESTSerializer.extend
  normalizePayload: (payload) ->
    # delete payload.facet_counts
    # delete payload.status

    years = payload.facet_counts.facet_fields.year
    it = 0
    facetYears = []
    for el, index in years by 2
      facetYears.push { id: it, year: el, count: years[index + 1] }
      it++

    meta = {
      numFound: payload.response.numFound
      start: payload.response.start
    }

    data = {
      movies: payload.response.docs
      facetYears: facetYears
    }
    Em.Logger.debug data
    return data

  extractMeta: (store, type, payload) ->
    Em.Logger.info 'in extractMeta..', payload
    if (payload and payload.response)
      store.metaForType type, { numFound: payload.response.numFound, start: payload.response.start }
      # delete payload.meta
      @