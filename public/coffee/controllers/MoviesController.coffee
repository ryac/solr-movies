App.MoviesController = Ember.ArrayController.extend

  queryParams: ['q','fq']

  # haveMovies: ( ->
  #   @get('model.length') > 0
  # ).property('model')

  # numMovies: ( ->
  #   @get('model.length')
  # ).property('model')

  # inflection: ( ->
  #   num = @get('numMovies')
  #   if (num > 1) then 'movies' else 'movie'
  # ).property('numMovies')

  actions:
    filterYear: (item) ->
      # Em.Logger.info 'here!'
      if (Em.isEmpty item)
        fQuery = undefined
      else if (item.get('year') is @get('year'))
        return
      else
        fQuery = 'year:' + item.get('year')

      @store.unloadAll('facet-year')
      @transitionToRoute 'movies', { queryParams: { fq: fQuery } }