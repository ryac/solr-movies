App.ApplicationController = Ember.ArrayController.extend

  # queryParams: ['q']
  # q: 'here'

  # haveMovies: ( ->
  #   @get('model.length') > 0
  # ).property('model')

  # numMovies: ( ->
  #   @get('model.length')
  # ).property('model')

  # inflection: ( ->
  #   num = @get('numMovies')
  #   if (num > 1) then 'movies' else 'movie'
  # ).property('numMovies')

  actions:
    search: ->
      @store.unloadAll('facet-year')
      @transitionToRoute 'movies', { queryParams: { q: @get('query'), fq: undefined } }