# App.Router.reopen {
#   location: 'history'
# }

App.Router.map ->
  @resource 'movies'

# App.IndexRoute = Ember.Route.extend
#   redirect: ->
#     @.transitionTo 'movies'

App.MoviesRoute = Ember.Route.extend
  queryParams:
      q:
        refreshModel: true
      fq:
        refreshModel: true
  model: (params) ->
    Em.Logger.info 'params', params
    if !Em.isEmpty(params.q) then @store.find 'movie', params

  setupController: (controller, model) ->
    @_super(controller, model)
    controller.set 'facetYear', @store.all 'facet-year'
    controller.set 'meta', @store.metadataFor('movie')

  actions:
    queryParamsDidChange: ->
      # @store.unloadAll('facet-year')
      Em.Logger.info 'in here..! queryParamsDidChange.'