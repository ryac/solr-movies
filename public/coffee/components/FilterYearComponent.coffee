App.FilterYearComponent = Ember.Component.extend
  tagName: 'div'
  classNames: ['panel']
  actions:
    filterYear: (item) ->
      @sendAction 'action', item

    clearFilter: ->
      Em.Logger.info 'remove filter..'
      @sendAction 'action', null