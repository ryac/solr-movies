App.FilterYearComponent = Ember.Component.extend({
  tagName: 'div',
  classNames: ['panel'],
  actions: {
    filterYear: function(item) {
      return this.sendAction('action', item);
    },
    clearFilter: function() {
      Em.Logger.info('remove filter..');
      return this.sendAction('action', null);
    }
  }
});
