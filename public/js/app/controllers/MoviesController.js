App.MoviesController = Ember.ArrayController.extend({
  queryParams: ['q', 'fq'],
  actions: {
    filterYear: function(item) {
      var fQuery;
      if (Em.isEmpty(item)) {
        fQuery = void 0;
      } else if (item.get('year') === this.get('year')) {
        return;
      } else {
        fQuery = 'year:' + item.get('year');
      }
      this.store.unloadAll('facet-year');
      return this.transitionToRoute('movies', {
        queryParams: {
          fq: fQuery
        }
      });
    }
  }
});
