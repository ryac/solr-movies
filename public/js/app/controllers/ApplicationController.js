App.ApplicationController = Ember.ArrayController.extend({
  actions: {
    search: function() {
      this.store.unloadAll('facet-year');
      return this.transitionToRoute('movies', {
        queryParams: {
          q: this.get('query'),
          fq: void 0
        }
      });
    }
  }
});
