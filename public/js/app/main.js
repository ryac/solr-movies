var App;

$(function() {
  return $(document).foundation();
});

App = Ember.Application.create({
  LOG_TRANSITIONS: true
});

App.ApplicationAdapter = DS.RESTAdapter.extend({
  namespace: 'api'
});

App.GenresTransform = DS.Transform.extend({
  serialize: function(value) {
    return value;
  },
  deserialize: function(value) {
    return Ember.A(value);
  }
});

App.ApplicationSerializer = DS.RESTSerializer.extend({
  normalizePayload: function(payload) {
    var data, el, facetYears, index, it, meta, years, _i, _len;
    years = payload.facet_counts.facet_fields.year;
    it = 0;
    facetYears = [];
    for (index = _i = 0, _len = years.length; _i < _len; index = _i += 2) {
      el = years[index];
      facetYears.push({
        id: it,
        year: el,
        count: years[index + 1]
      });
      it++;
    }
    meta = {
      numFound: payload.response.numFound,
      start: payload.response.start
    };
    data = {
      movies: payload.response.docs,
      facetYears: facetYears
    };
    Em.Logger.debug(data);
    return data;
  },
  extractMeta: function(store, type, payload) {
    Em.Logger.info('in extractMeta..', payload);
    if (payload && payload.response) {
      store.metaForType(type, {
        numFound: payload.response.numFound,
        start: payload.response.start
      });
      return this;
    }
  }
});
