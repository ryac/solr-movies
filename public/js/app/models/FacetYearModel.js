App.FacetYear = DS.Model.extend({
  year: DS.attr('string'),
  count: DS.attr('number')
});
