App.Movie = DS.Model.extend({
  title: DS.attr('string'),
  year: DS.attr('number'),
  mpaa_rating: DS.attr('string'),
  runtime: DS.attr('number'),
  poster: DS.attr('string'),
  link: DS.attr('string'),
  audience_score: DS.attr('number'),
  critics_score: DS.attr('number'),
  genres: DS.attr('genres')
});
