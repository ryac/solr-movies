App.Router.map(function() {
  return this.resource('movies');
});

App.MoviesRoute = Ember.Route.extend({
  queryParams: {
    q: {
      refreshModel: true
    },
    fq: {
      refreshModel: true
    }
  },
  model: function(params) {
    Em.Logger.info('params', params);
    if (!Em.isEmpty(params.q)) {
      return this.store.find('movie', params);
    }
  },
  setupController: function(controller, model) {
    this._super(controller, model);
    controller.set('facetYear', this.store.all('facet-year'));
    return controller.set('meta', this.store.metadataFor('movie'));
  },
  actions: {
    queryParamsDidChange: function() {
      return Em.Logger.info('in here..! queryParamsDidChange.');
    }
  }
});
